const { describe } = require('tape-plus')
const fs = require('fs')
const path = require('path')
const StreamCipher = require('..')
const { pipeline } = require('stream')
const { tmpdir } = require('os')
const { randomBytes } = require('crypto')
const mkdirp = require('mkdirp').sync
const rimraf = require('rimraf')

const hyperdrive = require('hyperdrive')

const testFile = path.join(path.resolve(__dirname), '../package.json')

describe('basic', (context) => {
  const testStorage = tmpdir() + randomBytes(4).toString('hex')

  context.beforeEach(t => {
    mkdirp(testStorage)
  })

  context.afterEach(t => {
    rimraf(testStorage, (err) => {
      if (err) throw err
    })
  })

  context('encrypt and decrypt file', (assert, next) => {
    const key = StreamCipher.key()
    const streamCipher = new StreamCipher(key)

    pipeline(
      fs.createReadStream(testFile).on('end', next),
      streamCipher.createEncryptStream(),
      streamCipher.createDecryptStream(),
      process.stdout,
      (err) => {
        assert.error(err, 'No error from pipeline')
      }
    )
  })

  context('put encrypted file in a hyperdrive', (assert, next) => {
    const key = StreamCipher.key()
    const streamCipher = new StreamCipher(key)

    const drive = hyperdrive(testStorage)
    const dest = drive.createWriteStream('package.json')
    pipeline(
      fs.createReadStream(testFile),
      streamCipher.createEncryptStream(),
      dest,
      (err) => {
        assert.error(err, 'No error from pipeline')
      }
    )
    dest.on('finish', () => {
      pipeline(
        drive.createReadStream('package.json').on('end', next),
        streamCipher.createDecryptStream(),
        process.stdout,
        (err) => {
          if (err) throw err
        }
      )
    })
  })

  context('random read', (assert, next) => {
    const key = StreamCipher.key()
    const streamCipher = new StreamCipher(key)

    pipeline(
      fs.createReadStream(testFile),
      streamCipher.createEncryptStream(),
      fs.createWriteStream(testFile + '.enc'),
      (err) => {
        assert.error(err, 'No error from pipeline')

        pipeline(
          fs.createReadStream(testFile + '.enc', { start: 5 }).on('end', next),
          streamCipher.createDecryptStream(5),
          process.stdout,
          (err) => {
            assert.error(err, 'No error from pipeline')
          }
        )
      }
    )
  })
})
