const app = require('express')()
const path = require('path')
// const fs = require('fs')
const ArtifactStore = require('dat-ahau')

const storage = path.join(path.resolve(__dirname), '../artifact-store/alice')
// const fileSize = fs.statSync(filePath).size
const artifactStore = new ArtifactStore(storage)

const fileToGet = 'big.mp4'
const fileSize = 1552036770
const encryptionKey = Buffer.from('b9471c0ff1759ce928ae1fdb97c512e64dd4fa96cc3d9ebf8312492c2fe4bfe0', 'hex')

async function main () {
  await artifactStore.ready()

  app.get('/', async function (req, res) {
    const header = {
      'Accept-Ranges': 'bytes',
      'Content-Type': 'video/mp4'
    }
    const range = req.range(fileSize)

    if (range) {
      const start = range[0].start
      const end = range[0].end || fileSize - 1

      header['Content-Range'] = `bytes ${start}-${end}/${fileSize}`
      header['Content-Length'] = (end - start) + 1
      res.writeHead(206, header)

      const readStream = await artifactStore.getReadStream(fileToGet, artifactStore.driveAddress, encryptionKey, { start, end })
      readStream.pipe(res)
    } else {
      header['Content-Length'] = fileSize
      res.writeHead(200, header)
      const readStream = await artifactStore.getReadStream(fileToGet, artifactStore.driveAddress, encryptionKey)
      readStream.pipe(res)
    }
  })

  app.listen(3001, function (err) {
    if (err) throw err
    console.log('http://localhost:3001')
  })
}

main()
