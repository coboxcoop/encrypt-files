const sodium = require('sodium-native')
const { Transform } = require('stream')
const OFFSET_CHUNK_SIZE = 65535

module.exports = class streamCipher {
  constructor (key, context) {
    // If we are not given something to make a nonce from,
    // derive from the key
    // This is only ok to do if this cipher is only ever used once
    context = context || key
    const nonce = genericHash(context).slice(0, sodium.crypto_stream_NONCEBYTES)
    this.rx = sodium.crypto_stream_xor_instance(nonce, key)
    this.tx = sodium.crypto_stream_xor_instance(nonce, key)
  }

  createStreamCipher (rxOrTx, offset) {
    const self = this

    if (offset) {
      const offsetBuffer = Buffer.alloc(OFFSET_CHUNK_SIZE)
      let bytesStillToRead = offset
      while (bytesStillToRead) {
        const toRead = offsetBuffer.slice(0, Math.min(bytesStillToRead, OFFSET_CHUNK_SIZE))
        self[rxOrTx].update(toRead, toRead)
        bytesStillToRead -= toRead.length
      }
    }

    return new Transform({
      transform (chunk, enc, callback) {
        self[rxOrTx].update(chunk, chunk)
        this.push(chunk)
        callback()
      }
    })
  }

  createEncryptStream (offset) {
    const stream = this.createStreamCipher('tx', offset)
    return stream
  }

  createDecryptStream (offset) {
    return this.createStreamCipher('rx', offset)
  }

  destroy () {
    this.tx.final()
    this.rx.final()
  }

  // static nonce () {
  //   const buf = Buffer.allocUnsafe(24)
  //   sodium.randombytes_buf(buf)
  //   return buf
  // }

  static key () {
    const buf = sodium.sodium_malloc(sodium.crypto_stream_KEYBYTES)
    sodium.randombytes_buf(buf)
    return buf
  }
}

function genericHash (msg) {
  const hash = sodium.sodium_malloc(sodium.crypto_generichash_KEYBYTES)
  sodium.crypto_generichash(hash, msg)
  return hash
}
