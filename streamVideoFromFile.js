const app = require('express')()
const path = require('path')
const fs = require('fs')

const filePath = path.join(path.resolve(__dirname), 'big.mp4')
const fileSize = fs.statSync(filePath).size

app.get('/', function (req, res) {
  const header = {
    'Accept-Ranges': 'bytes',
    'Content-Type': 'video/mp4'
  }
  const range = req.range(fileSize)

  if (range) {
    const start = range[0].start
    const end = range[0].end || fileSize - 1

    header['Content-Range'] = `bytes ${start}-${end}/${fileSize}`
    header['Content-Length'] = (end - start) + 1
    res.writeHead(206, header)

    fs.createReadStream(filePath, { start, end })
      .pipe(res)
  } else {
    header['Content-Length'] = fileSize
    res.writeHead(200, header)
    fs.createReadStream(filePath).pipe(res)
  }
})

app.listen(3001, function (err) {
  if (err) throw err
  console.log('http://localhost:3001')
})
