#!/usr/bin/env node
const http = require('http')
const fs = require('fs')
const path = require('path')
// const mime = require('mime')
const StreamCipher = require('.')

// Demonstrate streaming an encrypted video to the browser
// --have tried with a 1.5GB video and did not have problems

const commands = {
  serve () {
    const key = Buffer.from(process.argv[3], 'hex')

    const streamCipher = new StreamCipher(key)

    const server = http.createServer((req, res) => {
      res.writeHead(200, { 'Content-Type': 'video/mp4' })
      const stream = fs.createReadStream(path.join(path.resolve(__dirname), 'encrypted.mp4'))
      stream.pipe(streamCipher.createDecryptStream()).pipe(res)
    })
    server.listen(3001)
    console.log('http://localhost:3001')
  },
  encrypt () {
    const key = StreamCipher.key()
    console.log(`Encrypting file ${process.argv[3]} with key ${key.toString('hex')}`)
    console.log('Writing to ./encrypted.mp4...')

    const cipher = new StreamCipher(key)

    fs.createReadStream(process.argv[3])
      .pipe(cipher.createDecryptStream())
      .pipe(fs.createWriteStream('./encrypted.mp4'))
  }
}

if (typeof commands[process.argv[2]] !== 'function') {
  console.log('Usage:')
  console.log(`${process.argv[1]} encrypt <inputfile> - given an mp4 file, will make an encrypted copy called encrypted.mp4`)
  console.log(`${process.argv[1]} serve <key> - serve encrypted.mp4 over http, decrypting as it is read`)
  console.log('(copy the key from the output of the first command)')
  process.exit(1)
}
commands[process.argv[2]]()
